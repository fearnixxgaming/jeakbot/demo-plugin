package de.fearnixx.jeak.listeners;

import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.LocaleUnit;
import de.fearnixx.jeak.service.command.CommandException;
import de.fearnixx.jeak.service.command.CommandParameterException;
import de.fearnixx.jeak.service.command.ICommandContext;
import de.fearnixx.jeak.service.command.ICommandReceiver;
import de.fearnixx.jeak.service.locale.ILocalizationUnit;
import de.fearnixx.jeak.teamspeak.PropertyKeys;
import de.fearnixx.jeak.teamspeak.TargetType;
import de.fearnixx.jeak.teamspeak.cache.IDataCache;
import de.fearnixx.jeak.teamspeak.data.IClient;

import java.util.Collections;

public class HelpCommand implements ICommandReceiver {

    // Inject localization support (lang files)
    @Inject
    @LocaleUnit(value = "demo-greeting", defaultResource = "demoPlugin/lang.json")
    private ILocalizationUnit localization;

    // Inject data cache to retrieve clients by ID
    @Inject
    private IDataCache dataCache;

    /**
     * When the command is invoked, we send out the stored help message.
     */
    @Override
    public void receive(ICommandContext ctx) throws CommandException {

        // CommandExceptions are handled by the command service
        // and trigger a response to the message invoker.
        if (ctx.getTargetType() != TargetType.CLIENT) {
            throw new CommandParameterException("Please use that command in a private chat. :)");
        }

        // Good practice: Cover all bases. Even "impossible" ones!
        final Integer invokerId = ctx.getRawEvent().getProperty(PropertyKeys.TextMessage.SOURCE_ID)
                .map(Integer::parseInt)
                .orElseThrow(() -> new CommandException("Text message does not have an invoker!"));

        // Build the text message and send it.
        // Note: We do not care whether or not this was successful.
        IClient client = dataCache.getClientMap().get(invokerId);
        String message = localization.getContext(client.getCountryCode())
                .optMessage("help", Collections.emptyMap())
                .orElse("Help message missing");

        ctx.getRawEvent().getConnection().sendRequest(
                client.sendMessage(message)
        );
    }
}
