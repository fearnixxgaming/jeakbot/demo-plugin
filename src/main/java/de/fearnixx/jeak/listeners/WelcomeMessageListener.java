package de.fearnixx.jeak.listeners;

import de.fearnixx.jeak.event.IQueryEvent;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.Listener;
import de.fearnixx.jeak.reflect.LocaleUnit;
import de.fearnixx.jeak.service.locale.ILocalizationUnit;
import de.fearnixx.jeak.teamspeak.data.IClient;

import java.util.Collections;

public class WelcomeMessageListener {

    // Inject localization support (lang files)
    @Inject
    @LocaleUnit(value = "demo-greeting", defaultResource = "demoPlugin/lang.json")
    private ILocalizationUnit localization;

    /**
     * When a user joins the server, we greet him with a welcome message.
     */
    @Listener
    public void onUserJoin(IQueryEvent.INotification.IClientEnter event) {
        IClient client = event.getTarget();
        String message = localization.getContext(client.getCountryCode())
                .optMessage("welcome", Collections.emptyMap())
                .orElse("Welcome message missing!");

        event.getConnection().sendRequest(
                client.sendMessage(message)
        );
    }
}
