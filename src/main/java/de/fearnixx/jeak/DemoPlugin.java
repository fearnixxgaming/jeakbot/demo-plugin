package de.fearnixx.jeak;


import de.fearnixx.jeak.event.bot.IBotStateEvent;
import de.fearnixx.jeak.listeners.HelpCommand;
import de.fearnixx.jeak.listeners.WelcomeMessageListener;
import de.fearnixx.jeak.reflect.*;
import de.fearnixx.jeak.service.command.ICommandService;
import de.fearnixx.jeak.service.event.IEventService;
import de.fearnixx.jeak.util.Configurable;
import de.mlessmann.confort.api.IConfig;
import de.mlessmann.confort.api.IConfigNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Plugin to demonstrate the features of the JeakBot on a dedicated TeamSpeak server.
 */
@JeakBotPlugin(id = "demoplugin", version = "1.1.0")
public class DemoPlugin extends Configurable {

    // Create a logger using the SLF4J-API
    private static final Logger logger = LoggerFactory.getLogger(DemoPlugin.class);

    // Make our resources path for the default configuration a constant
    private static final String DEFAULT_CONF_PATH = "/demoPlugin/defaultConfig.json";

    // Inject the default configuration for this plugin
    // Will be in ``config/<pluginId>/<pluginId>.json
    @Inject
    @Config
    private IConfig configRef;

    // Inject the ICommandService so we can register commands
    @Inject
    private ICommandService commandService;

    // Inject the IEventServe so we can register additional listeners
    @Inject
    private IEventService eventService;

    // Allow injection into objects managed by us
    @Inject
    private IInjectionService injectionService;

    private WelcomeMessageListener welcomeListener = new WelcomeMessageListener();
    private HelpCommand helpListener = new HelpCommand();

    public DemoPlugin() {
        super(DemoPlugin.class);
    }

    /**
     * At the lifecycle event "Initialize", we load the configuration and register our commands.
     * Since this is our plugin class, this listener is automatically registered.
     */
    @Listener
    public void onInitialize(IBotStateEvent.IInitializeEvent event) {

        // If the configuration could not be loaded, abort the startup.
        if (!loadConfig()) {
            event.cancel();
            logger.warn("Failed to load plugin. Aborting startup!");
        }

        injectionService.injectInto(welcomeListener);
        injectionService.injectInto(helpListener);
        eventService.registerListener(welcomeListener);
        commandService.registerCommand("help", helpListener);
    }

    @Override
    protected IConfig getConfigRef() {
        return configRef;
    }

    @Override
    protected String getDefaultResource() {
        return DEFAULT_CONF_PATH;
    }

    @Override
    protected boolean populateDefaultConf(IConfigNode root) {
        return false;
    }

    @Override
    protected void onDefaultConfigLoaded() {
        saveConfig();
    }
}
